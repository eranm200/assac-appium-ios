# ///// this file will contain lines that can be used to assert with \\\\\


# ////Log-in\\\\
# Login system
log_in_assert = '#3201'

log_out_assert = '#3202'

auto_log_in_assert = '#3203'


# ////Call System\\\\
# call
call_started_assert = '#3204'

call_connected_assert = '#3205'

call_disconnected_assert = '#3206'

call_incoming_assert = '#3207'


# ////Chat System\\\\
# chat
message_sent_assert = '#32012'

message_received_assert = '#32013'

message_resent_assert = '#32014'

message_deleted_assert = '#32015'

# attachment
attachment_sent_assert = '#3208'

attachment_received_assert = '#32013'

attachment_resent_assert = '#32010'

attachment_deleted_assert = '#32011'


# ////notification system\\\\
message_notification_assert = '#32016'

attachment_notification_assert = '#32017'

missed_call_notification_assert = '#32018'


# chat msg content
message_content_from_plus_chat_ios = "hi from plus chat"

message_content_from_chat_ios = "hi from chat"







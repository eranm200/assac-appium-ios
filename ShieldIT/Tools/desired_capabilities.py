# ////////Main Desired Caps configuration\\\\\\\\
# check "deviceid" and "derivedDataPath" capabilities are updated!
main_iphone_8_desired_capabilities = {
                'app': 'com.assacnetworks.shieldit',
                'platformName': 'iOS',
                'platformVersion': '14.0',
                'deviceName': 'iPhone',
                'automationName': 'XCUITest',
                'udid': 'e37da25f0e7ebf626bf9ff131331b26ff5c47fa1',
                'deviceid': '192.168.178.101',
                'noReset': 'true',
                'wdaLocalPort': '8000',
                'newCommandTimeout': '200',
                # 'derivedDataPath': '/Users/eranmizrahi/Library/Developer/Xcode/DerivedData/WebDriverAgent-ciegwgvxzxdrqthilmrmczmqvrgu',
                'usePrebuiltWDA': 'true'
}

iphone_8_desired_capabilities = {
                'app': 'com.assacnetworks.shieldit',
                'platformName': 'iOS',
                'platformVersion': '14.0',
                'deviceName': 'iPhone',
                'automationName': 'XCUITest',
                'udid': 'f986bc2d9f4731fc2fa3c610c1a92d118fa02904',
                'deviceid': '192.168.178.101',
                'noReset': 'true',
                'wdaLocalPort': '8000',
                'newCommandTimeout': '200',
                'usePrebuiltWDA': 'true'
}

iphone_7_desired_capabilities = {
                'app': 'com.assacnetworks.shieldit',
                'platformName': 'iOS',
                'platformVersion': '14.0.1',
                'deviceName': 'Eran iPhone',
                'automationName': 'XCUITest',
                # 'deviceid': '192.168.178.102',
                'udid': '019a14bcbc1c3a416468ee01fa84311cdc5d3c54',
                'noReset': 'true',
                'wdaLocalPort': '8400',
                'newCommandTimeout': '200',
                # 'derivedDataPath': '/usr/local/lib/node_modules/appium/node_modules/appium-webdriveragent',
                'usePrebuiltWDA': 'true'
}

iphone_5_desired_capabilities = {
                'app': 'com.assacnetworks.shieldit',
                'platformName': 'iOS',
                'platformVersion': '12.4.8',
                'deviceName': 'iPhone',
                'automationName': 'XCUITest',
                'deviceid': '192.168.178.102',
                # 'udid': '67bdb78ca9816c2b135a750e26a47a2f7381d383',
                'noReset': 'true',
                'wdaLocalPort': '8400',
                'newCommandTimeout': '200',
}

# ////////SETUP Android device\\\\\\\\
galaxy_s8_desired_capabilities = {
                "app": "/Users/eranmorad/Desktop/apk/app-release.apk",
                "deviceName": "Galaxy S8",
                "automationName": "uiAutomator1",
                "platformVersion": "9",
                "platformName": "Android",
                "bundleid": "com.assacnetworks.shieldit",
                "udid": "ce11182b3955861b02",
                "noReset": 'true',
                "relaxedsecurity": 'true'
}


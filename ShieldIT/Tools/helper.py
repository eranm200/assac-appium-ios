import unittest
from Tools.android_helper import *
from Tools.desired_capabilities import *
from Tools.credentials import *
from Tools.assert_by_line import *
import time
from appium import webdriver


# ////////Desired Caps configuration\\\\\\\\
# insert your device desired capabilities profile.
main_device = iphone_8_desired_capabilities

second_device = iphone_7_desired_capabilities

third_device = galaxy_s8_desired_capabilities

# ////////Ports\\\\\\\\
# insert your device port
# make special ports routes var
main_port = 'http://127.0.0.1:4723/wd/hub'

second_port = 'http://127.0.0.1:4724/wd/hub'

third_port = 'http://127.0.0.1:4725/wd/hub'

# ////////appium driver\\\\\\\\
# insert your appium driver
main_appium_driver = webdriver.Remote(main_port, main_device)

second_appium_driver = webdriver.Remote(second_port, second_device)

# ////////message content configuration\\\\\\\\
message_content_from_plus_chat = "hi from plus chat"

message_content_from_chat = "hi from chat"


# ////////assertion method\\\\\\\\
# always make sure that the correct device udid is typed and a valid phrase.
def assert_by_line(driver, line):
    logs = driver.get_log('syslog')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    for log in second_set_of_log_messages:
        if line in log:
            print(log)
            is_registration_success = True
            return is_registration_success


# ////////iOS simple function\\\\\\\\
def go_to_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()

def chat_search_main_contact(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_accessibility_id('Search Contacts').click()
    driver.find_element_by_accessibility_id('Search Contacts').send_keys('AAiOS')
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="AAiOS "]').click()

def touch_middle_of_screen(driver):
    driver.swipe(start_x=130, start_y=230, end_x=130, end_y=230, duration=800)

def driver_close(driver):
    driver.close_app()

def driver_open(driver):
    driver.launch_app()

def driver_back(driver):
    driver.back()

def answer_call(driver):
    driver.swipe(start_x=331, start_y=62, end_x=331, end_y=62, duration=800)
    driver.swipe(start_x=275, start_y=550, end_x=275, end_y=550, duration=800)

def hang_up(driver):
    driver.find_element_by_accessibility_id('action hangup').click()

def toggle_wifi(driver):
    driver.swipe(start_x=208, start_y=730, end_x=209, end_y=441, duration=800)
    driver.find_element_by_accessibility_id('wifi-button').click()
    driver.swipe(start_x=730, start_y=208, end_x=441, end_y=209, duration=800)

def swipe_accessibility_menu(driver):
    driver.swipe(start_x=208, start_y=730, end_x=209, end_y=441, duration=800)

def rec_start(driver):
    driver.swipe(start_x=208, start_y=730, end_x=209, end_y=441, duration=800)
    driver.find_element_by_accessibility_id('Screen Recording').click()
    driver.swipe(start_x=730, start_y=208, end_x=441, end_y=209, duration=800)

def rec_stop(driver):
    driver.swipe(start_x=208, start_y=730, end_x=209, end_y=441, duration=800)
    driver.find_element_by_accessibility_id('Screen Recording').click()
    driver.swipe(start_x=730, start_y=208, end_x=441, end_y=209, duration=800)

# //////// call from dialer////////
# here you can add/delete new extensions numbers used for the dialer function test.
def call_from_dialer_9042(driver):
    driver.find_element_by_accessibility_id('9 WXYZ').click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('4 GHI').click()
    driver.find_element_by_accessibility_id('2 ABC').click()
    driver.find_element_by_accessibility_id('call icon').click()
    hang_up(driver)

def call_from_dialer_1004(driver):
    driver.find_element_by_accessibility_id("1  ").click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('4 GHI').click()
    driver.find_element_by_accessibility_id('call icon').click()

def call_from_dialer_1005(driver):
    driver.find_element_by_accessibility_id("1  ").click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('5 JKL').click()
    driver.find_element_by_accessibility_id('call icon').click()

def call_from_dialer_1006(driver):
    driver.find_element_by_accessibility_id("1  ").click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('6 MNO').click()
    driver.find_element_by_accessibility_id('call icon').click()


def call_from_dialer_1019(driver):
    driver.find_element_by_accessibility_id("1  ").click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id("1  ").click()
    driver.find_element_by_accessibility_id('9 WXYZ').click()
    driver.find_element_by_accessibility_id('call icon').click()
    hang_up(driver)

def call_from_dialer_1050(driver):
    driver.find_element_by_accessibility_id("1  ").click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('5 JKL').click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('call icon').click()


def call_from_dialer_2015(driver):
    driver.find_element_by_accessibility_id('2 ABC').click()
    driver.find_element_by_accessibility_id('0 +').click()
    driver.find_element_by_accessibility_id('1  ').click()
    driver.find_element_by_accessibility_id('5 JKL').click()
    driver.find_element_by_accessibility_id('call icon').click()


# this variables use is for easier switch between extensions numbers.
call_from_dialer_device_ios_1 = call_from_dialer_1050
call_from_dialer_device_ios_2 = call_from_dialer_1019
call_from_dialer_device_android_1 = call_from_dialer_1006

# ////////Log / IN / OUT\\\\\\\\
def log_in(driver):
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]').send_keys(main_user_extension)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]').send_keys(main_user_password)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther').click()
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton').click()
    time.sleep(2)
    driver.find_element_by_accessibility_id('LOGIN').click()

def log_out(driver):
    time.sleep(4)
    driver.swipe(start_x=42, start_y=45, end_x=42, end_y=45, duration=800)
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="Logout"]').click()
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="Logout"]').click()

# ////////chat actions only to ios device\\\\\\\\
# chat actions
def write_and_send_msg_ios(driver, message):
    driver.find_element_by_accessibility_id('chatto.inputbar.inputfield.text').send_keys(message)
    touch_middle_of_screen(driver)
    driver.find_element_by_accessibility_id('Send').click()

def send_attachment_ios(driver):
    driver.find_element_by_accessibility_id('photos.chat.input.view').click()
    time.sleep(4)
    driver.swipe(start_x=200, start_y=530, end_x=200, end_y=530, duration=800)
    driver.find_element_by_accessibility_id('Send Image').click()
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="Send Image"]').click()

def send_message_from_plus_chat_ios(driver):
    chat_search_main_contact(driver)
    time.sleep(2)
    write_and_send_msg_ios(driver, message_content_from_plus_chat)

def send_message_from_chat_ios(driver):
    go_to_chat(driver)
    time.sleep(2)
    # select AAiOS contact
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell').click()
    write_and_send_msg_ios(driver, message_content_from_chat)

def send_attachment_from_chat_ios(driver):
    go_to_chat(driver)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell').click()
    time.sleep(3)
    send_attachment_ios(driver)

def resend_message_from_chat_ios(driver, message):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="Errrann "]').click()
    driver.find_element_by_accessibility_id('chatto.inputbar.inputfield.text').send_keys(message)
    toggle_wifi(driver)
    driver.find_element_by_accessibility_id('chatto.inputbar.button.send').click()
    time.sleep(3)
    toggle_wifi(driver)
    time.sleep(10)
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="base message failed icon"]').click()
    time.sleep(3)
    driver.find_element_by_accessibility_id('Resend message').click()

def delete_msg_from_chat_ios(driver, message):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="Errrann "]').click()
    driver.find_element_by_accessibility_id('chatto.inputbar.inputfield.text').send_keys(message)
    driver.find_element_by_accessibility_id('chatto.inputbar.button.send').click()
    time.sleep(3)
    driver.swipe(start_x=321, start_y=116, end_x=321, end_y=116, duration=1000)
    driver.find_element_by_accessibility_id('Delete').click()
    driver.find_element_by_accessibility_id('Delete').click()


# ////////chat actions only to android device\\\\\\\\
def send_message_from_plus_chat_android(driver, message):
    time.sleep(2)
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="AAndroid "]').click()
    driver.find_element_by_xpath('//XCUIElementTypeTextView[@name="chatto.inputbar.inputfield.text"]/XCUIElementTypeTextView').click()
    driver.find_element_by_accessibility_id('chatto.inputbar.inputfield.text').send_keys(message)
    driver.find_element_by_accessibility_id('chatto.inputbar.button.send').click()

def send_message_from_chat_android(driver, message):
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell').click()
    driver.find_element_by_xpath('//XCUIElementTypeTextView[@name="chatto.inputbar.inputfield.text"]/XCUIElementTypeTextView').click()
    driver.find_element_by_xpath('//XCUIElementTypeTextView[@name="chatto.inputbar.inputfield.text"]/XCUIElementTypeTextView').send_keys(message)
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="chatto.inputbar.button.send"]').click()

def send_attachment_from_chat_android(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell').click()
    time.sleep(3)
    driver.find_element_by_accessibility_id('photos.chat.input.view').click()
    time.sleep(4)
    driver.swipe(start_x=200, start_y=530, end_x=200, end_y=530, duration=800)

def resend_message_from_chat_android(driver, message):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="AAndroid "]').click()
    driver.find_element_by_accessibility_id('chatto.inputbar.inputfield.text').send_keys(message)
    toggle_wifi(driver)
    driver.find_element_by_accessibility_id('chatto.inputbar.button.send').click()
    time.sleep(3)
    toggle_wifi(driver)
    time.sleep(10)
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="base message failed icon"]').click()
    driver.find_element_by_accessibility_id('Resend message').click()

# ////////call system actions only for ios device\\\\\\\\
def call_from_contacts_ios(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="Iphone 5 "]').click()
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="assac: 1004"]').click()

def call_from_chat_ios(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]').click()
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="small call icon"]').click()

def call_from_plus_chat_ios(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="Iphone 5 "]').click()
    time.sleep(2)
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="small call icon"]').click()


# ////////call system actions only for android device\\\\\\\\
def call_from_contacts_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]').click()
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="assac: 1006"]').click()

def call_from_chat_android(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell').click()
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="small call icon"]').click()

def call_from_plus_chat_android(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_accessibility_id('Compose').click()
    driver.find_element_by_xpath('//XCUIElementTypeApplication[@name="ShieldiT"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]').click()
    time.sleep(2)
    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="small call icon"]').click()



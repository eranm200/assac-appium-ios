import unittest
import traceback
import time
import logging
from Tests.call_system.call_ios import CallTestIos
from Tests.call_system.call_android import CallTestAndroid
from Tests.log_in_system.log_in import LoginTest
from Tests.log_in_system.auto_log_in import AutoLogin
from Tests.chat_system.message_ios import MessageTestIos
from Tests.chat_system.message_android import MessageTestAndroid
from Tests.network_changes.handleIP import HandleIpTest
from Tools import HTMLTestRunner
import subprocess

def runTest():
  times = 1
  timeout = time.time() + 60*1  # 24h = 60*60*24
  timestr = time.strftime('%Y_%m_%d_%H.%M.%S', time.localtime(time.time()))

  filename = "./logs/"+timestr+".html"
  with open(filename, 'wb') as f:
      runner = HTMLTestRunner.HTMLTestRunner(
        stream=f,
        title=u'Test Report: {0}'.format(times),
        description=u'24 test case, test by TG'
      )
      logging.info('Test Times: {0}'.format(times))

      times += 1
      runner.run(suite())

def suite():
  suite = unittest.TestSuite()
  tests = [
    LoginTest("test_login"),
    AutoLogin("test_auto_log_in"),
    MessageTestIos('test_resend_msg_to_ios'),
    MessageTestIos("test_send_msg_from_plus_in_chat_to_ios"),
    MessageTestIos("test_send_msg_from_chat_to_ios"),
    MessageTestIos('test_send_attachment_to_ios'),
    MessageTestAndroid('test_resend_msg_to_ios'),
    MessageTestAndroid('test_send_msg_from_plus_in_chat_to_android'),
    MessageTestAndroid('test_send_msg_from_chat_to_android'),
    MessageTestAndroid('test_send_attachment_to_ios'),
    CallTestIos("test_call_to_ios_from_dialer"),
    CallTestIos("test_call_to_ios_from_chat"),
    CallTestIos("test_call_to_ios_from_contact"),
    CallTestIos('test_call_to_ios_from_plus_chat'),
    CallTestIos('test_hangup_call_to_ios'),
    CallTestAndroid("test_call_to_android_from_dialer"),
    CallTestAndroid("test_call_to_android_from_contact"),
    CallTestAndroid("test_call_to_android_from_plus_chat"),
    CallTestAndroid("test_call_to_android_from_chat"),
    LoginTest('test_logout'),
    # HandleIpTest('network_change_in_call'),
  ]
  suite.addTests(tests)
  return suite


if __name__ == "__main__":
  try:
    runTest()
  except Exception as e:
    logging.info('Exception: {0}'.format(e))
    logging.debug('Exception: {0}'.format(traceback.format_exc()))
  finally:
    print('Please check the Reports.')
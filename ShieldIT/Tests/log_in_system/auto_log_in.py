from Tools.helper import *
from Tools.assert_by_line import *

class AutoLogin(unittest.TestCase):

    @classmethod
    def setUp(cls):
        cls.driver = main_appium_driver
        main_appium_driver.implicitly_wait(10)

    def test_auto_log_in(self):
        driver_open(self.driver)
        time.sleep(5)
        self.assertTrue(assert_by_line(self.driver, auto_log_in_assert))
        driver_close(self.driver)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AutoLogin)
    unittest.TextTestRunner(verbosity=2).run(suite)
from Tools.helper import *
from Tools.assert_by_line import *

class LoginTest(unittest.TestCase):

    @classmethod
    def setUp(cls):
        cls.driver = main_appium_driver
        main_appium_driver.implicitly_wait(10)

    def test_login(self):
        driver_open(self.driver)
        log_in(self.driver)
        time.sleep(10)
        self.assertTrue(assert_by_line(self.driver, log_in_assert))
        time.sleep(3)
        driver_close(self.driver)

    def test_logout(self):
        driver_open(self.driver)
        log_out(self.driver)
        time.sleep(5)
        self.assertTrue(assert_by_line(self.driver, log_out_assert))
        driver_close(self.driver)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(LoginTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
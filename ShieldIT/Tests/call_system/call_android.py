from Tools.helper import *
from Tools.assert_by_line import *

class CallTestAndroid(unittest.TestCase):
    @classmethod
    def setUp(self):
        self.driver = main_appium_driver
        main_appium_driver.implicitly_wait(10)

    def test_call_to_android_from_dialer(self):
        driver_open(self.driver)
        time.sleep(5)
        call_from_dialer_device_android_1(self.driver)
        time.sleep(20)
        self.assertTrue(assert_by_line(self.driver, call_connected_assert))
        driver_close(self.driver)

    def test_call_to_android_from_contact(self):
        driver_open(self.driver)
        time.sleep(4)
        call_from_contacts_android(self.driver)
        time.sleep(20)
        self.assertTrue(assert_by_line(self.driver, call_connected_assert))
        driver_close(self.driver)

    def test_call_to_android_from_chat(self):
        driver_open(self.driver)
        call_from_chat_android(self.driver)
        time.sleep(20)
        self.assertTrue(assert_by_line(self.driver, call_connected_assert))
        driver_close(self.driver)

    def test_call_to_android_from_plus_chat(self):
        driver_open(self.driver)
        call_from_plus_chat_android(self.driver)
        time.sleep(15)
        self.assertTrue(assert_by_line(self.driver, call_connected_assert))
        driver_close(self.driver)

    # def test_hangup_call_to_android(self):
    #     driver_open(self.driver)
    #     driver_open(self.driver1)
    #     call_from_dialer_1004(self.driver)
    #     time.sleep(7)
    #     answer_call(self.driver1)
    #     time.sleep(5)
    #     hang_up(self.driver1)
    #     time.sleep(15)
    # self.assertTrue(assert_by_line(self.driver, message_sent_assert))
    #     driver_close(self.driver)
    #     driver_close(self.driver1)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CallTestAndroid)
    unittest.TextTestRunner(verbosity=2).run(suite)

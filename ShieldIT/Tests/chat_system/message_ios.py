from Tools.helper import *


class MessageTestIos(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.driver = main_appium_driver
        main_appium_driver.implicitly_wait(10)

    def test_send_msg_from_plus_in_chat_to_ios(self):
        driver_open(self.driver)
        send_message_from_plus_chat_ios(self.driver)
        time.sleep(10)
        self.assertTrue(assert_by_line(self.driver, message_sent_assert))
        time.sleep(3)
        driver_close(self.driver)

    def test_send_msg_from_chat_to_ios(self):
        driver_open(self.driver)
        send_message_from_chat_ios(self.driver)
        time.sleep(10)
        self.assertTrue(assert_by_line(self.driver, message_sent_assert))
        driver_close(self.driver)

    def test_send_attachment_to_ios(self):
        driver_open(self.driver)
        send_attachment_from_chat_ios(self.driver)
        time.sleep(30)
        self.assertTrue(assert_by_line(self.driver, attachment_sent_assert))
        driver_close(self.driver)

    def test_resend_msg_to_ios(self):
        driver_open(self.driver)
        resend_message_from_chat_ios(self.driver, "hi from chat")
        time.sleep(20)
        self.assertTrue(assert_by_line(self.driver, message_resent_assert))
        driver_close(self.driver)

    def test_delete_msg_ios(self):
        driver_open(self.driver)
        delete_msg_from_chat_ios(self.driver, "delete me please")
        self.assertTrue(assert_by_line(self.driver, message_deleted_assert))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(MessageTestIos)
    unittest.TextTestRunner(verbosity=2).run(suite)





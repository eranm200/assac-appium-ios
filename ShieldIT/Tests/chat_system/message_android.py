from Tools.helper import *


class MessageTestAndroid(unittest.TestCase):
    @classmethod
    def setUp(self):
        self.driver = main_appium_driver
        main_appium_driver.implicitly_wait(10)

    def test_send_msg_from_plus_in_chat_to_android(self):
        driver_open(self.driver)
        send_message_from_plus_chat_android(self.driver, "hi from plus_chat")
        time.sleep(15)
        self.assertTrue(assert_by_line(self.driver, message_sent_assert))
        driver_close(self.driver)

    def test_send_msg_from_chat_to_android(self):
        driver_open(self.driver)
        send_message_from_chat_android(self.driver, "hi from chat")
        time.sleep(15)
        self.assertTrue(assert_by_line(self.driver, message_sent_assert))
        driver_close(self.driver)

    def test_send_attachment_to_ios(self):
        driver_open(self.driver)
        send_attachment_from_chat_android(self.driver)
        time.sleep(15)
        self.assertTrue(assert_by_line(self.driver, attachment_sent_assert))
        driver_close(self.driver)

    def test_resend_msg_to_ios(self):
        driver_open(self.driver)
        resend_message_from_chat_android(self.driver, "hi from chat")
        time.sleep(20)
        self.assertTrue(assert_by_line(self.driver, message_resent_assert))
        driver_close(self.driver)




